window.onload = async () => {
  //start the webgazer tracker
  await webgazer
    .setRegression("ridge") /* currently must set regression and tracker */
    .setTracker("TFFacemesh")
    .setGazeListener((data, elapsedTime) => {
      /* data is an object containing an x and y key which are the x and y prediction coordinates (no bounds limiting) */
      /* elapsed time in milliseconds since webgazer.begin() was called */
      console.log({ data, elapsedTime });
    })
    .begin();

  webgazer
    .showVideoPreview(true) /* shows all video previews */
    .showPredictionPoints(
      true,
    ) /* shows a square every 100 milliseconds where current prediction is */
    .applyKalmanFilter(
      true,
    ); /* Kalman Filter defaults to on. Can be toggled by user. */
};

window.onbeforeunload = () => webgazer.end();
