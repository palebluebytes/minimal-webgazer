{
  description = "A Nix-flake-based Node typescript development environment";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs }:
    let
      inherit (builtins) attrValues;

      overlays = [ (final: prev: { nodejs = prev.nodejs_21; }) ];

      supportedSystems =
        [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];

      forEachSupportedSystem = f:
        nixpkgs.lib.genAttrs supportedSystems
        (system: f { pkgs = import nixpkgs { inherit overlays system; }; });

      devTools = pkgs:
        attrValues {
          # Nix
          inherit (pkgs) nil nixfmt node2nix;

          # Language Servers
          inherit (pkgs) yaml-language-server vscode-langservers-extracted;
          inherit (pkgs.nodePackages)
            typescript-language-server bash-language-server prettier typescript;
        };

    in {
      devShells = forEachSupportedSystem ({ pkgs }: {
        default = pkgs.mkShell {
          packages =
            attrValues { inherit (pkgs) nodejs python3 pkg-config cairo; }
            ++ devTools pkgs;
        };
      });
    };
}
