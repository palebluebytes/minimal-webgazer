import basicSsl from "@vitejs/plugin-basic-ssl";

export default {
  build: {
    outDir: "public",
  },
  publicDir: "public-files",
  plugins: [basicSsl()],
};
